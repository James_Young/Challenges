from app import db


class CommentModel(db.Model):
    blog_id = db.Column(db.String(80), db.ForeignKey('blog_model.blog_id'), nullable=False)
    comment_id = db.Column(db.Integer, primary_key=True, nullable=False, unique=True)
    date_posted = db.Column(db.DateTime, nullable=False)
    text = db.Column(db.String(1000), nullable=False)

