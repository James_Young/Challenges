from app import db
from datetime import datetime as date


class BlogModel(db.Model):
    blog_id = db.Column(db.String(20), primary_key=True, nullable=False, unique=True)
    date_posted = db.Column(db.DateTime, nullable=False, default=date.utcnow())
    title = db.Column(db.String(200), nullable=False)
    content = db.Column(db.String(2000), nullable=False)
    comments = db.relationship('CommentModel', backref=db.backref('blog', lazy='select'), lazy='joined')