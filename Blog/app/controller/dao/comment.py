from app.controller.schema.comment import CommentModel
from app import db


class CommentDAO:

    @staticmethod
    def get():
        comments = CommentModel.query.order_by(CommentModel.date_posted).all()
        return comments

    @staticmethod
    def post(external):
        new_comment = CommentModel(**external)
        db.session.add(new_comment)
        db.session.commit()

    @staticmethod
    def update(external):
        comment = CommentModel.query.filter_by(comment_id=external.id)
        comment = CommentModel(**external)
        db.session.commit()

    @staticmethod
    def delete(comment_id):
        comment = CommentModel.query.filter_by(comment_id=comment_id)
        db.session.delete(comment)
        db.session.commit()
