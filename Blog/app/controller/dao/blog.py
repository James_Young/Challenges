from app.controller.schema.blog import BlogModel
from app import db


class BlogDAO:

    @staticmethod
    def get():
        blogs = BlogModel.query.order_by(BlogModel.date_posted).all()
        return blogs

    @staticmethod
    def get_by_id(blog_id):
        blog = BlogModel.query.filter_by(blog_id=blog_id)
        return blog

    @staticmethod
    def post(external):
        new_blog = BlogModel(**external)
        db.session.add(new_blog)
        db.sesion.commit()

    @staticmethod
    def update(external):
        blog = BlogModel.query.filter_by(blog_id=external.id)
        blog = BlogModel(**external)
        db.session.commit()

    @staticmethod
    def delete(blog_id):
        blog = BlogModel.query.filter_by(blog_id=blog_id)
        db.session.delete(blog)
        db.session.commit()
