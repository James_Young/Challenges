

def find_parent(comment, comment_list):
    if len(comment_list) == 0:
        return
    for i, com in enumerate(comment_list):
        if str(comment["parent_comment_id"]) == str(com["id"]):
            comment_list[i]["replies"].append(comment)
            return
        else:
            find_parent(comment, comment_list[i]["replies"])