from datetime import datetime


class Comment:

    def __init__(self, user, text, blog_id, parent_comment_id=None):

        self.user = user
        self.text = text
        self.upVotes = 0
        self.downVotes = 0
        self.date = datetime.now().strftime("%d %b %Y")
        self.time = datetime.now().strftime("%I:%M %p")
        self.replies = []
        self.blog_id = blog_id

        self.parent_comment_id = parent_comment_id

    def get(self):
        return {
            "user": self.user,
            "text": self.text,
            "upVotes": self.upVotes,
            "downVotes": self.downVotes,
            "date": self.date,
            "time": self.time,
            "blog_id": self.blog_id,
            "replies": self.replies,
            "parent_comment_id": self.parent_comment_id
        }