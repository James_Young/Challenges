class Blog:

    @property
    def template_name(self):
        return self._template_name

    @template_name.setter
    def template_name(self, value):
        self._template_name = value

    @property
    def context(self):
        return self._context

    @context.setter
    def context(self, value):
        if isinstance(value, dict):
            self._context = value
        else:
            raise Exception('Blog context must be of dictionary type.')

    def __init__(self, blogdict):
        self.template_name = blogdict['template_name']
        self.context = blogdict['context']

    def filepath(self):
        return 'blogs/' + self.template_name + '/' + self.template_name + '.html'

    def get(self):
        return {"template_name":self.template_name, "context":self.context}