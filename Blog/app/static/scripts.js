function showReplies(id) {
    var x = document.getElementById(id);
    var x_button = document.getElementById("button"+id);
    if (x.style.display === "none") {
        x.style.display = "block";
        x_button.innerHTML = "Hide Replies";
    } else {
        x.style.display = "none";
        x_button.innerHTML = "Show Replies";
    }
}

function hide_all_reply_forms() {

}

function show_reply_form(id) {
    var x = document.getElementById("reply"+String(id));

    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";

    }
}

function add_comment_form(id) {
    var x = document.getElementById("add"+id);
    var x_button = document.getElementById("button"+id);
    if (x.style.display === "none") {
        x.style.display = "block";
        x_button.innerHTML = "-";
    } else {
        x.style.display = "none";
        x_button.innerHTML = "+";

    }
}