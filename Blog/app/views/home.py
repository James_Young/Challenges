from app import webapp
from app.models.blog import Blog
import json
from flask import render_template

@webapp.route('/', methods=['GET'])
def get_home():
    try:
        with open('app/static/blogs/home.json') as jsonfile:
            blog = Blog(json.load(jsonfile))
            return render_template('homepage/home.html', **blog.context)
    except IOError as e:
        return str(e)
