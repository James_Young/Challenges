from app import webapp
from app.models.blog import Blog
import json
from flask import render_template

@webapp.route('/blogs', methods=['GET'])
def get_blogsplash():
    return render_template('blogs/blog_splash/blog_splash.html')
