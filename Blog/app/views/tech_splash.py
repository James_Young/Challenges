from app import webapp
from app.models.blog import Blog
import json
from flask import render_template

@webapp.route('/tech', methods=['GET'])
def get_techsplash():
    return render_template('trump_tech/trump_tech_splash/tech_splash.html')
