from app import webapp
from app.models.blog import Blog
from app.controller.dao.blog import BlogDAO
from app.controller.dao.comment import CommentDAO
from app.controller.helpers import find_parent
from flask import render_template, redirect, url_for
import json

from app.models.comment import Comment
from flask import request
from datetime import datetime as date


@webapp.route('/comment/<blog_id>', methods=['POST'])
def post_comment(blog_id):

    blog = BlogDAO.get_by_id(blog_id)
    comments = CommentDAO.get()

    external = dict(
        blog_id=blog_id,
        comment_id=len(comments) + 1,
        text=request.form['text'],
        date_posted=date.utcnow(),
        blog=blog.first()
    )
    CommentDAO.post(external)
    return redirect(url_for('get_blog', blog_id=blog_id))


@webapp.route('/blogs/<blog_id>', methods=['GET', 'POST'])
def get_blog(blog_id):
    blog = BlogDAO.get_by_id(blog_id)
    if blog.first() is not None:
        return render_template('/blogs/basic_db_template/basic_db_template.html', posts=blog)
    try:
        f = open('app/static/blogs/' + blog_id + '.json', 'r')
        blog = Blog(json.load(f))
        f.close()

        if request.method == 'POST':
            user = request.form["user"]
            text = request.form["text"]
            parent_comment_id = request.form["parent_comment_id"]

            comment = Comment(user, text, blog_id, parent_comment_id=parent_comment_id).get()
            blog.context["comment_count"] += 1
            comment["id"] = str(blog.context["comment_count"])
            if parent_comment_id == 'null':
                blog.context["comments"].append(comment)
            else:
                find_parent(comment, blog.context["comments"])

        f = open('app/static/blogs/' + blog_id + '.json', 'w+')
        json.dump(blog.get(), f, sort_keys=True, indent=4)
        f.close()
        return render_template(blog.filepath(), **blog.context)

    except IOError as e:
        return str(e)
