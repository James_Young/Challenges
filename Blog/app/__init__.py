from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

project_directory = os.path.dirname(os.path.abspath(__file__))
db_fullpath = "sqlite:///{}".format(os.path.join(project_directory, "static", "database", "webapp.db"))


webapp = Flask(__name__, static_url_path='/app/static')
webapp.config['SQLALCHEMY_DATABASE_URI'] = db_fullpath
db = SQLAlchemy(webapp)

from app.controller.schema import *
import app.controller
import app.views

db.create_all()
