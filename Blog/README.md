# Week Challenge

Applying machine learning to Trump's ridiculous vernacular.

### Develop

    git clone git@gitlab.com:blog-team/week-challenge.git
    cd week-challenge
    virtualenv -p python3.6 venv
    source venv/bin/activate
    pip install -r requirements.txt

