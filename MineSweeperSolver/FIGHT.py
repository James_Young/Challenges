from minesweeper import *

from mySolver import *



def playgame2(n_plays):
    gridsize = 9
    numberofmines = 10

    currgrid = [[' ' for i in range(gridsize)] for i in range(gridsize)]

    grid = []
    flags = []
    starttime = 0

    helpmessage = ("Type the column followed by the row (eg. a5). "
                   "To put or remove a flag, add 'f' to the cell (eg. a5f).")

    #showgrid(currgrid)
    #print(helpmessage + " Type 'help' to show this message again.\n")

    n_wins = 0

    while True:

        suggestion, bombs, bombs_grid = solve()

        minesleft = numberofmines - bombs
        #minesleft = numberofmines - len(flags)


        #prompt = input('Enter the cell ({} mines left): '.format(minesleft))
        #print('Enter the cell ({0} mines left): {1}'.format(minesleft, suggestion))

        prompt = suggestion
        result = parseinput(prompt, gridsize, helpmessage + '\n')

        message = result['message']
        cell = result['cell']

        if cell:
            #print('\n\n')
            rowno, colno = cell
            currcell = currgrid[rowno][colno]
            flag = result['flag']

            if not grid:
                grid, mines = setupgrid(gridsize, cell, numberofmines)
            if not starttime:
                starttime = time.time()

            if flag:
                # Add a flag if the cell is empty
                if currcell == ' ':
                    currgrid[rowno][colno] = 'F'
                    flags.append(cell)
                # Remove the flag if there is one
                elif currcell == 'F':
                    currgrid[rowno][colno] = ' '
                    flags.remove(cell)
                else:
                    message = 'Cannot put a flag there'

            # If there is a flag there, show a message
            elif cell in flags:
                message = 'There is a flag there'

            elif grid[rowno][colno] == 'X':
                #print('Game Over\n')
                #showgrid(grid)
                print("Game : Lose")
                print(bombs_grid)
                showgrid(grid)

                return 0

            elif currcell == ' ':
                showcells(grid, currgrid, rowno, colno)

            else:
                message = "That cell is already shown"

            if minesleft == 0:
            #if set(flags) == set(mines):
                minutes, seconds = divmod(int(time.time() - starttime), 60)
                # print(
                #     'You Win. '
                #     'It took you {} minutes and {} seconds.\n'.format(minutes,
                #                                                       seconds))



                print("Game: Win")
                print(bombs_grid)
                showgrid(grid)

                return 1


N_games = 10
n_wins = 0
for i in range(N_games):
    n_wins += playgame2(N_games)

print("{0} Games".format(N_games))
print("{0} Wins".format(n_wins))


