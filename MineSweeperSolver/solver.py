import numpy as np

importGrid = open("current_map.txt", "r")
tmpGrid = importGrid.readlines()

grid = [i.strip() for i in tmpGrid]
grid = [i.split('|') for i in grid]

for i in range(9):
    for j in range(9):
        try:
            grid[i][j] = int(grid[i][j])
        except:
            grid[i][j] = -1

for x in grid:
    del x[9]

grid = np.array(grid, dtype=int)
# print(grid[0][1])

# print(grid)

grid_np = np.array(grid)

risk_grid = np.ones((9, 9)) + 1

bombs_grid = np.zeros((9,9))


def count_surrounding_empty(grid, row, column):
    a = get_surrounding_empty(grid, row, column)
    return len(a)


def check_if_bomb(row, column):
    return bombs_grid[row][column] == 1.0


def count_neighbouring_bombs(row, column):
    indices = get_surrounding_empty(grid, row, column)
    count = 0
    for [row, column] in indices:

        if check_if_bomb(row, column):
            count += 1
    return count

def weight(grid_np, row, column):
    numInBox = grid_np[row][column]
    numEmptyNeighbours = count_surrounding_empty(grid_np, row, column)
    bombs = count_neighbouring_bombs(row, column)



    return (numInBox - bombs) / (numEmptyNeighbours)


def get_surrounding_empty(grid, row, column):
    def get_neighbouring_indices(row, column):
        indices = []
        for i in range(-1, 2):
            for j in range(-1, 2):
                if i == 0 and j == 0: continue
                if row + i < 0 or row + i > 8: continue
                if column + j < 0 or column + j > 8: continue

                indices.append([row + i, column + j])
        return indices

    test_neighbours = get_neighbouring_indices(row, column)

    # eliminate indices of non-empty cells
    neighbours = []
    for [x, y] in test_neighbours:

        if grid[x][y] < 0:
            # Cell is empty
            neighbours.append([x, y])

    return neighbours


def update_element_risk(grid_np, risk_grid, i, j):

    if count_surrounding_empty(grid_np, i, j) > 0:
        tmp = get_surrounding_empty(grid_np, i, j)
        for k in tmp:

            weightNum = weight(grid_np, i, j)

            if check_if_bomb(k[0], k[1]):
                risk_grid[k[0]][k[1]] = 1

            elif risk_grid[k[0]][k[1]] == 2:
                risk_grid[k[0]][k[1]] = weightNum
            else:

                risk_grid[k[0]][k[1]] = np.max([weightNum, risk_grid[k[0]][k[1]]])


def update_risk(grid_np):
    risk_grid = np.ones((9,9)) + 1
    for i in range(9):
        for j in range(9):
            if not grid_np[i][j] < 0:
                update_element_risk(grid_np, risk_grid, i, j)
    return risk_grid

def get_min(risk_grid):
    ind = np.unravel_index(np.argmin(risk_grid, axis=None), risk_grid.shape)
    ind = list(ind)
    ind[0] = ind[0] + 1
    alpha = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i']
    ind[1] = alpha[ind[1]]
    return ind

def update_bombs(risk_grid):
    bombs = np.zeros((9, 9))
    for i in range(9):
        for j in range(9):
            if risk_grid[i][j] == 1.0:
                bombs[i][j] = 1
            else:
                bombs[i][j] = 0
    return bombs

def find_bombs(risk_grid):
    bombs = np.zeros((9,9), dtype=str)
    for i in range(9):
        for j in range(9):
            if risk_grid[i][j] == 1.0:
                bombs[i][j] = "B"
            else:
                bombs[i][j] = " "
    return bombs

risk_grid = update_risk(grid_np)
bombs_grid = update_bombs(risk_grid)
t = get_min(risk_grid)
print(t)
risk_grid = update_risk(grid_np)
bombs_grid = update_bombs(risk_grid)
t = get_min(risk_grid)
print(t)
risk_grid = update_risk(grid_np)
bombs_grid = update_bombs(risk_grid)
t = get_min(risk_grid)
print(t)
risk_grid = update_risk(grid_np)
bombs_grid = update_bombs(risk_grid)
t = get_min(risk_grid)
print(t)
