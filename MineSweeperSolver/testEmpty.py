import numpy as np

importGrid = open("current_map.txt", "r")
tmpGrid = importGrid.readlines()

grid = [i.strip() for i in tmpGrid]
grid = [i.split('|') for i in grid]

for i in range(9):
    for j in range(9):
        try:
            grid[i][j] = float(grid[i][j])
        except:
            grid[i][j] = -1

for x in grid:
    del x[9]

grid = np.array(grid, dtype=float)



def get_empty_neighbours(row, column):

    def get_neighbouring_indices(row, column):
        indices = []
        for i in range(-1, 2):
            for j in range(-1, 2):
                if i == 0 and j == 0: continue
                if row + i < 0 or row + i > 8: continue
                if column + j < 0 or column + j > 8: continue

                indices.append([row + i, column + j])
        return indices

    test_neighbours = get_neighbouring_indices(row, column)

    # eliminate indices of non-empty cells
    neighbours = []
    for [x, y] in test_neighbours:

        if grid[x][y] < 0:

            # Cell is empty
            neighbours.append([x, y])


    return neighbours


emptyNeighbourIndices = get_empty_neighbours(0,0)

np.ar