from os import listdir
path = "data/"

# Process files
for file in listdir(path):
    data = []
    f = open(path + file)
    for line in f:
        line = line.strip("\n")
        if len(line) == 0: 
            continue
        data.append(line.split("\t"))
    f.close()
    f = open("processed_files/" + file, "w+")
    for d in data:
        f.write("\t".join(d[1:]) + "\n")
    f.close()
    

