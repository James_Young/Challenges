import re


class DataBase:

    def __init__(self, entry_keys):
        self.dict = {}
        self.entry_keys = entry_keys

    def add_entry(self, ID, entry_list):
        if not self.__is_ID_unique(ID):
            return False
        if len(entry_list) != len(self.entry_keys):
            print("Help, Keys", len(entry_list), len(self.entry_keys))
            return False
        
        self.dict[ID] = dict(zip([x.get_name() for x in self.entry_keys] , entry_list))
        return True

    def remove_entry(self, ID):
        if not ID in self.dict.keys(): return False
        del self.dict[ID]
        return True

    def __is_ID_unique(self, ID):
        return not ID in self.dict.keys()        

    def get_entry_by_ID(self, ID):
        if ID in self.dict.keys(): return self.dict[ID]
        else: return None
   
    def get_fields(self):
        return self.entry_keys
    
    def get_field(self, field):
        results = {}
        for key, val in self.dict.items():
            results[key] = val[field]
        return results

    def show(self):
        for key, val in self.dict.items():
            print(key, val)

class FieldKey:

    def __init__(self, name, entry_type=str, required=False):
        self.__entry_key = (name, entry_type, required)

    def get_type(self):
        return self.__entry_key[1]

    def get_name(self):
        return self.__entry_key[0]

    def is_required(self):
        return self.__entry_key[2]


class TitleDB(DataBase):

    def __init__(self):
        entry_list = []
        entry_list.append(FieldKey("Title"))
        DataBase.__init__(self, entry_list)


class DescriptionDB(DataBase):

    def __init__(self):
        entry_list = []
        entry_list.append(FieldKey("Genre"))
        entry_list.append(FieldKey("Description"))
        DataBase.__init__(self, entry_list)


class RatingsDB(DataBase):

    def __init__(self):
        entry_list = []
        entry_list.append(FieldKey("Rating", float))
        entry_list.append(FieldKey("Votes", int))
        entry_list.append(FieldKey("Revenue", float))
        entry_list.append(FieldKey("Metascore", int))
        DataBase.__init__(self, entry_list)


class DetailsDB(DataBase):

    def __init__(self):
        entry_list = []
        entry_list.append(FieldKey("Director"))
        entry_list.append(FieldKey("Actors"))
        entry_list.append(FieldKey("Year", int))
        entry_list.append(FieldKey("Runtime", int))        
        DataBase.__init__(self, entry_list)


class MovieDB():
    
    def __init__(self):
        self.Titles = TitleDB()
        self.Desc = DescriptionDB()
        self.Ratings = RatingsDB()
        self.Details = DetailsDB()
        self.DB_list = [self.Titles, self.Desc, self.Ratings, self.Details]
        self.search_dict = {}
        
    def set_titles(self, title_list):
        for i, title in enumerate(title_list):
            self.Titles.add_entry(i, [title])
            
            self.add_search_reference(title, i)


    def show(self, ID=None):
        if ID != None:
            print(self.Titles.get_entry_by_ID(ID))
            print(self.Desc.get_entry_by_ID(ID))
            print(self.Ratings.get_entry_by_ID(ID))
            print(self.Details.get_entry_by_ID(ID))
            print("")
            return 
        for i in range(400):
            print(self.Titles.get_entry_by_ID(i))
            print(self.Desc.get_entry_by_ID(i))
            print(self.Ratings.get_entry_by_ID(i))
            print(self.Details.get_entry_by_ID(i))
            print("") 
    
    def load_desc(self, file_name, header=0, delimiter="\t"):
        f = open(file_name)
        for i, line in enumerate(f):
            if i == header: continue
            line = line.strip("\n")
            line = list(line.split(delimiter))
            #print("Adding line: ", line)
            self.Desc.add_entry(i, line[1:])
            
            for item in line:
                self.add_search_reference(item, i)

    def load_details(self, file_name, header=0, delimiter="\t"):
        f = open(file_name)
        for i, line in enumerate(f):
            if i == header: continue
            line = line.strip("\n")
            line = list(line.split(delimiter))
            #print("Adding line: ", line)
            self.Details.add_entry(i, line[1:])
    
            for item in line:
                self.add_search_reference(item, i)
        
    def load_ratings(self, file_name, header=0, delimiter="\t"):
        f = open(file_name)
        for i, line in enumerate(f):
            if i == header: continue
            line = line.strip("\n")
            line = list(line.split(delimiter))
            #print("Adding line: ", line)
            self.Ratings.add_entry(i, line[1:])
            
            for item in line:
                self.add_search_reference(item, i)

    def add_search_reference(self, phrase, ID):
        words = re.split('\W+', phrase)
        for word in words:
            if word not in self.search_dict.keys():
                self.search_dict[word] = []
            if ID in self.search_dict[word]:
                continue
            self.search_dict[word].append(ID)
     
    def find(self, keyword):
        IDS = self.search_dict[keyword]
        for ID in IDS:
            self.show(ID)
