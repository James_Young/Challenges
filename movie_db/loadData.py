from os import listdir
from DataBase import *
path = "processed_files/"


def set_up_MDB(MDB):
    file_name = "Challenge_Day_2_movie.desc"

    f = open(path + file_name)
    header = 0
    titles = []
    for i, line in enumerate(f):
        line = line.strip("\n")
        if i == header:
            continue
        if len(line) == 0:
            continue
        titles.append(line.split("\t")[0])

    MDB.set_titles(titles)
    MDB.load_desc(path + "Challenge_Day_2_movie.desc")
    MDB.load_details(path + "Challenge_Day_2_movie.people_time")
    MDB.load_ratings(path + "Challenge_Day_2_movie.ratings")
    
    return MDB
