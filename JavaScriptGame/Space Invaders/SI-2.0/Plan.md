# Plan

Project Layout

/Game
     -> app.js
     -> /userInterface (view)
     -> /gameManager (controller)
     -> /models
     
        component = {
                attributes: {},
                methods: {
                    actions: {},
                    reactions: {}
                        }
                    }
        
        -> Player
            -> Attributes
                -> Shape
                    -> Shape Attributes
                    -> Color
                
                -> Physics
                    -> Mass
                    -> Speed
                    -> Acceleration
                
                -> Max Lives, int 
                -> Lives, int (0 to 'Max Lives')
                -> Score, int
                -> Status
                    -> state 
                        -> object {state: boolean / int} 
                            ('alive': true, 'moving': false, 'hit': 3, 'firing': false)
                            int is number of frames in which the state will remain active
                
            -> Methods
                -> Actions
                    -> Move
                        -> Left
                        -> Right
                    -> Fire Missile
                -> Reactions
                    -> Is Hit
                    
            
        -> Alien
            -> Attributes
                -> Shape
                    -> Shape Attributes
                    -> Color
                
                -> Physics
                    -> Mass
                    -> Speed
                    -> Acceleration
                
                -> Max Lives, int 
                -> Lives, int (0 to 'Max Lives')
                -> Status
                    -> state 
                        -> object {state: boolean / int} 
                            ('alive': true, 'moving': false, 'hit': 3, 'firing': false)
                            int is number of frames in which the state will remain active
                
            -> Methods
                -> Actions
                    -> Move
                        -> Left
                        -> Right
                        -> Up
                        -> Down
                    -> Drop Bomb
                -> Reactions
                    -> Is Hit
        
        -> Projectile
            -> Attributes
                -> Shape
                    -> Shape Attributes
                    -> Color
                
                -> Physics
                    -> Mass
                    -> Speed
                    -> Acceleration
                
                -> Status
                    -> state 
                        -> object {state: boolean / int} 
                            ('active': true, 'moving': false, 'impacted': 3)
                -> Type
                
            -> Methods
                -> Actions
                    -> Move
                        -> Left
                        -> Right
                        -> Up
                        -> Down
                -> Reactions
                    -> Impact
        
        -> Baracade
        -> World
        -> Game Statistics
        
            -> Attributes
                -> Level
                
                -> Score
                